####Spuštění (pro XAMPP)
    localhost/<project_folder>/www
####Credentials
    username: admin
    password: admin
####API
    /api/getName/<id>

#Použité technologie

##Framework
    Nette 2.4

##Verze PHP
    7.1

##IDE
    PhpStorm

##VCS
    git + BitBucket
    
##Operační systém
    MS Windows 10

##Databáze
    MySQL + adminer

###Config
    /app/config/config.neon

###Data
    database.sql
Původně jsem chtěl použít ORM Doctrine2, dříve jsem se s ORM už setkal (Django framework), ale s Doctrine se teprve seznamuji, takže jsem vsadil na jistotu - Nette Database

##Ostatní
    composer

