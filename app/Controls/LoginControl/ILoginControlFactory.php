<?php

namespace App\Controls;

interface ILoginControlFactory
{
    /** @return LoginControl */
    public function create();
}