<?php

namespace App\Controls;

use Nette;
use Nette\Application\UI\Control;

class LoginControl extends Control
{

    private $user;

    public $onLogin;

    public function __construct(Nette\Security\User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    public function render()
    {
        $this->template->setFile(__DIR__."/LoginControl.latte");
        $this->template->render();
    }

    protected function createComponentForm()
    {
        $form = new Nette\Application\UI\Form;
        $form->addText("username","Username")->setRequired();
        $form->addPassword("password","Password")->setRequired();
        $form->addSubmit("send","Login");
        $form->onSuccess[] = [$this,"processForm"];
        return $form;

    }


    public function processForm($form,$values){
        try {
            $this->user->login($values->username, $values->password);
        } catch (Nette\Security\AuthenticationException $e) {
            $this->presenter->flashMessage("Bad credentials!","warning");
        }
        $this->onLogin($this,$this->user->getIdentity());
    }
}