<?php

namespace App\Controls;

use Nette;

class ProductControl extends \Nette\Application\UI\Control
{

    private $database;
    const TABLE = "products";
    const COLUMN_ID = "id";


    public function __construct(Nette\Database\Context $database)
    {
        parent::__construct();
        $this->database = $database;
    }

    public function render()
    {
        $this->template->setFile(__DIR__."/ProductControl.latte");
        $this->template->render();
    }

    protected function createComponentForm()
    {
        $form = new \Nette\Application\UI\Form;
        $form->addHidden("id");
        $form->addText("name","Product name")->setRequired();
        $form->addSubmit("send","Save");
        $form->onSuccess[] = [$this,"processForm"];
        return $form;
    }

    public function processForm($form,$values)
    {
        if($values->id === ""){
            $this->database->table(self::TABLE)->insert([
               "title"=>$values->name
            ]);
        $this->presenter->flashMessage("Product successfully added","success");
        }
        else{
            $this->database->table(self::TABLE)->where(self::COLUMN_ID,$values->id)->update([
                "title"=>$values->name
            ]);
            $this->presenter->flashMessage("Product successfully updated","success");
        }
        $this->redirect("this");
    }
}