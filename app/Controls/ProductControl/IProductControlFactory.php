<?php

namespace App\Controls;

interface IProductControlFactory
{
    /** @return ProductControl */
    public function create();

}