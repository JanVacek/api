<?php

namespace App\RestModule\Model;

use Nette;

class Product
{

    private $database;

    const TABLE = "products";
    const COLUMN_ID = "id";

    /**
     * Product constructor.
     * @param Nette\Database\Context $database
     */
    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }


    /**
     * @param int $id
     */
    public function getProduct(int $id)
    {

        return $this->database->table(self::TABLE)->where(self::COLUMN_ID,$id)->fetch();

    }
}