<?php

namespace App\RestModule\Presenters;

use App\RestModule\Model\Product;
use Nette;

class ProductPresenter extends Nette\Application\UI\Presenter
{

    private $database;

    const TABLE = "products";
    const COLUMN_ID = "id";

    public function __construct(Nette\Database\Context $database)
    {
        parent::__construct();
        $this->database = $database;
    }

    public function actionGetName($id)
    {
        $manager = new Product($this->database);
        $result = $manager->getProduct($id);
        if(!$result){
            $result = [];
            array_push($result,["status"=>"404"]);
        }
        else{
            $result = iterator_to_array($result);
            array_push($result,["status"=>"200"]);
        }
        $this->sendResponse(new Nette\Application\Responses\JsonResponse($result));

    }
}