<?php


namespace App\Presenters;

use App\RestModule\Model\Product;
use Nette;
use App\Controls\IProductControlFactory;

class AdminPresenter extends BasePresenter
{

    private $IProductControlFactory;

    const TABLE = "products";

    /**
     * AdminPresenter constructor.
     * @param Nette\Database\Context $database
     * @param IProductControlFactory $IProductControlFactory
     */
    public function __construct(Nette\Database\Context $database, IProductControlFactory $IProductControlFactory)
    {
        parent::__construct($database);
        $this->IProductControlFactory = $IProductControlFactory;
    }

    /**
     * @throws Nette\Application\AbortException
     */
    public function startup()
    {
        parent::startup();
        if(!$this->getUser()->loggedIn){
            $this->redirect("Login:login");
        }
    }

    public function renderDefault()
    {
        $this->template->list = $this->database->table(self::TABLE);
        $id = $this->getParameter("id");
        if($id){
            $product = new Product($this->database);
            $this["productForm"]["form"]->setDefaults(["name"=>$product->getProduct($id)["title"],"id"=>$id]);
        }
    }

    /**
     * @return \App\Controls\ProductControl
     */
    protected function createComponentProductForm(){

        $control = $this->IProductControlFactory->create();
        return $control;
    }

}