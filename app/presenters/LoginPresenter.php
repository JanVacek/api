<?php

namespace App\Presenters;

use App\Controls\ILoginControlFactory;
use App\Controls\LoginControl;
use Nette;

class LoginPresenter extends BasePresenter
{

    private $IloginControlFactory;

    /**
     * LoginPresenter constructor.
     * @param Nette\Database\Context $database
     * @param ILoginControlFactory $ILoginControlFactory
     */
    public function __construct(Nette\Database\Context $database, ILoginControlFactory $ILoginControlFactory){
        parent::__construct($database);
        $this->IloginControlFactory = $ILoginControlFactory;
    }

    /**
     * @throws Nette\Application\AbortException
     */
    public function renderLogin()
    {
        if($this->getUser()->isLoggedIn() && $this->getUser()->roles["admin"]==1)
            $this->redirect("Admin:default");

    }


    /**
     * @return LoginControl
     */
    protected function createComponentLoginForm()
    {

        $control = $this->IloginControlFactory->create();
        $control->onLogin[] = function (LoginControl $control,$role){
          if($role->roles["admin"] == 1)
              $this->redirect("Admin:default");
        };
        return $control;
    }

    /**
     * @throws Nette\Application\AbortException
     */
    public function actionLogout()
    {
        $this->getUser()->logout();
        $this->redirect("Login:login");
    }
}