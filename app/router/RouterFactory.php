<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
		$router[] = new Route('', 'Login:login');
		$router[] = new Route('admin', 'Admin:default');
		$router[] = new Route('login/logout', 'Login:logout');
		$router[] = new Route('api/getName/<id>', 'Rest:Product:getName');
//		$router[] = new Route('api/getName/<id>', [
//		    "module"=>"Rest",
//            "presenter"=>"Product",
//            "action"=>"getName"
//        ]);
		return $router;
	}
}
